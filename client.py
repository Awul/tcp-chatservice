#!/usr/bin/python3


import socket


HOST = "127.0.0.1"
PORT = 5008
MAX_BYTES_READ = 1024
MESSAGE = "Moin"


s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
s.send(MESSAGE.encode("utf-8"))
msg = s.recv(MAX_BYTES_READ)
s.close()


print(msg.decode('utf-8'))
